package FileSystemApp;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class TimeFileWriter {
	static final String type = ".txt";

	public static void writeTimes(long[] array, String filename) throws IOException {
		File file = new File(filename+".csv");
		file.createNewFile();
		FileWriter fw = new FileWriter(file);
		fw.write("File #, Time(ms)"+System.getProperty("line.separator"));
		for(int j = 0; j < array.length; j++)
		{
			fw.write((j+1)+","+array[j]+System.getProperty("line.separator"));
			fw.flush();
		}
		fw.close();
	}
}