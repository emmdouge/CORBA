package FileSystemApp;

import org.omg.CosNaming.*;

import java.util.ArrayList;
import java.util.Properties;

import org.omg.CORBA.*;

/**
 * A simple client that just gets a
 * @author Merlin
 *
 */
public class FileSystemClient
{
	static FileSystem fileSystemImpl;

	/**
	 * Just do each operation once
	 * @param args ignored
	 */
	public static void main(String args[])
	{
		try
		{
			Properties props = new Properties();
	        
			// create and initialize the ORB
			ORB orb = ORB.init(args, props);

			// get the root naming context
			org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
			
			// Use NamingContextExt instead of NamingContext. This is
			// part of the Interoperable naming Service.
			NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

			// resolve the Object Reference in Naming
			String name = "FileSystem";
			fileSystemImpl = FileSystemHelper.narrow(ncRef.resolve_str(name));

			System.out.println("Obtained a handle on server object: " + fileSystemImpl);
			System.out.println(fileSystemImpl.sayHello());
			System.out.println("finished hello starting to read file");
			
			
			int numTimes = 100;
			
			ArrayList<String> names = new ArrayList<String>();
			names.add("small");
			names.add("med");
			names.add("big");
			
			long beforeSend;
			long afterSend;

			long[] times;
			
			for(int i = 0; i < names.size(); i++) {
				times = new long[numTimes];
				for(int j = 0; j < numTimes; j++) {
					beforeSend = System.currentTimeMillis();
					//asks server for file
					System.out.println(fileSystemImpl.readFile(names.get(i)+".txt"));
					afterSend = System.currentTimeMillis();
					long timeTaken = afterSend-beforeSend;
					times[j] = timeTaken;
				}
				TimeFileWriter.writeTimes(times, names.get(i)+"Times");
				System.out.println("\n\n\n\n CREATED FILE!!! \n\n\n\n");
			}
			
			// This is how we would shut down the server
			//fileSystemImpl.shutdown();
		} catch (Exception e)
		{
			System.out.println("ERROR : " + e);
			e.printStackTrace(System.out);
		}
	}

}
