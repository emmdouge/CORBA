package FileSystemApp;


/**
* FileSystemApp/FileSystemOperations.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from ../FileSystem/FileSystem.idl
* Wednesday, November 29, 2017 2:02:57 PM EST
*/

public interface FileSystemOperations 
{
  String sayHello ();
  String readFile (String title);
  void shutdown ();
} // interface FileSystemOperations
