#!/bin/bash
cd src
dd if=/dev/zero of=small.txt bs=100KiB count=1
dd if=/dev/zero of=med.txt bs=500KiB count=1
dd if=/dev/zero of=big.txt bs=1M count=1